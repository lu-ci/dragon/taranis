# To Do

## Frontend

### Landing
- Overall navigation revision
- Remove navigation "buy now" button hover background
- Hero background image creation
- Hero text downscaling
- Header text color revision
- Remove service card shadows between the cards

### PLP
- Create mobile display
- Reverse manufacturer and model (model on top, manuf. on underneath)
- Turn "RSD" into subtext
- Add price range number field `@change` event to change the slider value
- Sub-navigation bar to select "Weapons" or "Ammunition"

![subnav](/docs/img/plp_subnav.png)
- Discount mode: items can have discount prices, causing the old price to be shown with a strikethrough above the new one

![discount](/docs/img/plp_discount.png)

### PDP
***Unstarted***

### Security
The frontend needs to implement the LCValidator (A-Zero) token system. It's already made in TS so it can be grabbed from the main LC site.

## Backend

### Models
- General

#### User
- id: int
- name: str
- admin: bool

Example:
```json
{
  "id": 0,
  "name": root,
  "admin": true
}
```

#### Weapon
- id: int
- manufacturer: str*
- model: str
- price: float
- discount_price: float
- stock: int
- description: dict (`{lang: str}`)
- images: list(`[str]`)
- type: int (hardcoded/predefined)*
- caliber: str*

`*` indicates fields to be changed to dedicated models at a later date.

Example:
```json
{
  "id": 0,
  "manufacturer": "Zastava Arms",
  "model": "99",
  "price": 72000.0,
  "discount_price": 60000.0,
  "stock": 4,
  "description": {
    "en": "half gun, half jammed",
    "sr": "polu pistolj, polu zaglavljen"
  },
  "images": [
    "cz_99_001.png",
    "cz_99_002.png",
    // ...,
    "cz_99_999.png"
  ],
  "type": 1,
  "caliber": "9x19mm"
}
```

#### Ammunition
- id: int
- manufacturer: str*
- name: str
- price: float
- discount_price: float
- stock: int
- description: dict (`{lang: str}`)
- images: list(`[str]`)
- caliber: str*

`*` indicates fields to be changed to dedicated models at a later date.

Example:
```json
{
  "id": 0,
  "manufacturer": "Prvi Partizan Uzice",
  "model": "FMJ 9x19mm 50kom",
  "price": 45.0,
  "discount_price": 42.0,
  "stock": 501,
  "description": {
    "en": "bullyeet",
    "sr": "oce metak boc"
  },
  "images": [
    "ppu_fmj_001.png",
    "ppu_fmj_002.png",
    // ...,
    "ppu_fmj_999.png"
  ],
  "caliber": "9x19mm"
}
```

### CRUD
- General
- Data validator

### Security
The backend LCValidator hasn't been made for Py yet, so Alex has to make that.

## Control Panel
***Unstarted***
