FROM node:16-alpine

# create destination directory
RUN mkdir -p /srv/app
WORKDIR /srv/app

# update and install dependency
RUN apk update && apk upgrade && apk add bash

# copy the app, note .dockerignore
COPY . /srv/app
RUN yarn install
RUN yarn build
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

# start the app
CMD [ "yarn", "start" ]
