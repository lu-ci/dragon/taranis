import fetch from 'node-fetch'
import slugify from 'slugify'

const hostname = 'https://dragonshooting.rs'
const title = 'Dragon Shooting'
const description = 'Indoor shooting range, weapons and ammunition store, and workshop with a focus on sport shooting and discipline.'
const logo = `${hostname}/Logo.png`

const i18nConfig = {
  baseURL: hostname,
  locales: [
    { code: 'sr', iso: 'sr_RS', file: 'sr.ts', name: 'Srpski' },
    { code: 'en', iso: 'en_US', file: 'en.ts', name: 'English' }
  ],
  lazy: true,
  langDir: '@/lang',
  defaultLocale: 'sr',
  vueI18n: {
    fallbackLocale: 'sr'
  },
}

const locales = []
i18nConfig.locales.forEach((locale) => {
  locales.push(locale.code)
})

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: true,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'server',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Dragon Shooting - Streljana Zmaj',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'description', content: description },
      { name: 'format-detection', content: 'telephone=no' },
      // Twitter
      { name: 'twitter:card', content: 'summary' },
      { name: 'twitter:creator', content: '@AXAz0r' },
      { name: 'twitter:description', content: description },
      { name: 'twitter:image', content: logo },
      { name: 'twitter:site', content: '@streljanazmaj' },
      { name: 'twitter:title', content: title },
      // OpenGraph
      { property: 'og:title', content: title },
      { property: 'og:url', content: hostname },
      { property: 'og:image', content: logo },
      { property: 'og:image:secure', content: logo },
      { property: 'og:description', content: description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'shortcut icon', type: 'image/png', href: logo }
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // 'buefy/dist/buefy.css',
    'boxicons/css/boxicons.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/buefy' },
    { src: '@/plugins/boxicons', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxt/image',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/i18n',
    '@nuxtjs/sitemap',
    '@nuxtjs/google-gtag',
    'nuxtjs-microsoft-clarity'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  i18n: i18nConfig,

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  sitemap: {
    hostname,
    i18n: {
      locales
    }
  },

  generate: {
    async routes () {
      const uris = []
      const resp = await fetch('https://tohru.luciascipher.com/api/weapons?populate=manufacturer')
      const data = await resp.json()
      data.data.forEach((weapon) => {
        const slug = slugify(`${weapon.id}-${weapon.attributes.manufacturer.data.attributes.name}-${weapon.attributes.name}`, { lower: true, remove: /[*+~.()'"!:@]/g })
        // uris.push(`/product/${slug}`)
        i18nConfig.locales.forEach((locale) => {
          const code = locale.code
          if (code === i18nConfig.defaultLocale) {
            uris.push(`/product/${slug}`)
          } else {
            uris.push(`/${code}/product/${slug}`)
          }
        })
      })
      return uris
    }
  },
  'google-gtag': {
    id: 'G-1NZHJG47SY',
    config: {
      anonymize_ip: false,
      send_page_view: true,
      linker: {
        domains: ['dragonshooting.rs', 'www.dragonshooting.rs', 'shooting.rs', 'www.shooting.rs']
      },
    },
    debug: true,
    disableAutoPageTrack: false
  },
  microsoftClarity: {
    id: 'd1blgcz6rb'
  }
}
